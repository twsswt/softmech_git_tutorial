# A Tutorial on Git and GitLab for the SoftMech PhD Student and RA Training Day

Tuesday, 11th December 2017

Tim Storer (timothy.storer@glasgow.ac.uk) and Tom Wallis

The latest version of the slides is available [here](https://gitlab.com/twsswt/softmech_git_tutorial/-/jobs/artifacts/master/download?job=SLIDY-HTML).

You can also browse a notes style version on GitLab [here](https://gitlab.com/twsswt/softmech_git_tutorial/blob/master/working.md).


